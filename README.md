# Détection de sentiments dans des commentaires
## PS6 - Vial Jérôme


Infos générales
---------------

- **Etudiant/stagiaire** : [Jérôme Vial](https://gitlab.forge.hefr.ch/jerome.vial1) - jerome.vial@edu.hefr.ch
- **Superviseur** : [Didier Crausaz](https://gitlab.forge.hefr.ch/didier.crausaz) - didier.crausaz@hefr.ch
- **Superviseur** : [Karl Daher](https://gitlab.forge.hefr.ch/karl.daher) - karl.daher@hefr.ch
- **Professeur** : [Elena Mugellini](https://gitlab.forge.hefr.ch/elena.mugellini) - elena.mugellini@hefr.ch

Contexte
--------

Ce projet de détection de sentiments dans des commentaires a été développé durant un projet de semestre effectué durant le semestre de printemps 2021.


Description
-----------

Le but de ce projet est de développer et rechercher un moyen d'extraire des sentiments présents dans des commentaires rédigés en français.


Contenu
-------

Ce dépôt contient toute la documentation relative au projet dans le dossier `docs/`. Le code du projet est dans le dossier `code/`.
