import matplotlib.pyplot as plt
import numpy as np

# Camembert ----------------------------------------------------------------
# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'Négatif', 'Neutre', 'Positif'
sizes = [23, 17, 60]
explode = (0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()
# Thermometre -------------------------------------------------------------
# Bar plot
x = 1	
fig, ax = plt.subplots(figsize=(12, 6))

emotion = ['Negative', 'Neutral', 'Positive']
percentage = [-23,17,60]
plt.title('General emotion')
plt.ylabel('Positivity')
plt.ylim([-100, 100])
ax.set_xlim(0,3)
plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
plt.axhline(y=0, color='black', linestyle='-')

ax.bar(x, percentage[2], width=0.35, color='g')
ax.bar(x, percentage[0], width=0.35, color='r')
plt.show()

# Emoji ------------------------------------------------------------------
# Emoji result
percentage = [23,17,60] # neg, neut, pos
max_value = max(percentage)
max_index = percentage.index(max_value)

if(max_index == 0):
	print("\U0001F625")
elif(max_index == 1):
	print("\U0001F610")
elif(max_index == 2):
	print("\U0001F603")
# Pourcentage --------------------------------------------------------------
percentage = [23,17,60]
print(str(percentage[2]) +"% des utilisateurs ont un sentiment positif face à cette vidéo")