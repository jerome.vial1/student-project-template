from googleapiclient.discovery import build 
import os
api_key = 'AIzaSyAlMdYX37k-iuE8IeSJpVjylEwQ2fKwHUo'

# Enter video id 
video_id = "KLNvgcWzn74"

def video_comments(video_id): 
	# empty file for storing comments
	outputFile = open("comments_"+video_id+".txt", "w", encoding='utf-8')

	# empty dictionnary to store the data
	commentsDict = []

	# empty list for storing reply 
	replies = [] 

	# creating youtube resource object 
	youtube = build('youtube', 'v3', 
					developerKey=api_key) 

	# retrieve youtube video results 
	video_response=youtube.commentThreads().list( 
	part='snippet,replies', 
	videoId=video_id 
	).execute() 

	# iterate video response 
	while video_response: 	
		# extracting required info 
		# from each result object 
		for item in video_response['items']:
			# Extracting comments 
			comment = item['snippet']['topLevelComment']['snippet']['textDisplay'] 
			commentEntrie = {"comment": comment, 'replies': []}
			
			# counting number of reply of comment 
			replycount = item['snippet']['totalReplyCount'] 

			# if reply is there 
			if replycount>0: 
				
				# iterate through all reply 
				for reply in item['replies']['comments']: 
					
					# Extract reply 
					reply = reply['snippet']['textDisplay'] 
					
					# Store reply is list 
					replies.append(reply) 
					commentEntrie['replies'].append(reply)
					
			# print comment with list of reply 
			print(comment, replies, end = '\n\n')
			outputFile.write("%s" % comment)
			outputFile.write("%s\n" % replies)
			commentsDict.append(commentEntrie)
			# empty reply list 
			replies = [] 

		# Again repeat 
		if 'nextPageToken' in video_response:
			p_token = video_response['nextPageToken']
			video_response = youtube.commentThreads().list(part = 'snippet,replies',videoId = video_id,pageToken = p_token).execute()
		else:
			break
			
	outputFile.close()
	print(commentsDict)

def file_cleaner(video_id):
	replacements = {'<br />':'', '&#39;':'\'', '[]':'','&quot;':'"','&lt;':'<','&gt;':'>'}
	lines = []
	with open('comments_'+video_id+'.txt', encoding="utf8") as infile:
		for line in infile:
			for src, target in replacements.items():
				line = line.replace(src, target)
			lines.append(line)
	with open('comments_'+video_id+'.txt', 'w', encoding="utf8") as outfile:
		for line in lines:
			outfile.write(line)

def write_source_comments(video_id):
	line = 'source : https://www.youtube.com/watch?v=' + video_id
	with open('comments_'+video_id+'.txt', 'r+', encoding="utf8") as f:
		content = f.read()
		f.seek(0, 0)
		f.write(line.rstrip('\r\n') + '\n' + content)

# Call function 
video_comments(video_id)
file_cleaner(video_id)
write_source_comments(video_id);