# Ce script permet d'analyser des entrées d'un dataset avec PyFeel
# et d'analyser le pourcentage de réussite.

# importing library
import pandas as pd
from pyFeel import Feel
import numpy as np

np_load_old = np.load
# modify the default parameters of np.load
np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)

# Then loading csv file
df = pd.read_csv('french_tweets.csv')

# Store the number of analyse that match the polarity of the dataset +/- rtol
correct = 0

# Analyse each Tweet (0 -> 768308 first entries are negatives Tweet, then positives)
for index, row in df.iloc[768308:778308].iterrows():
   test = Feel(row['text'])
   guess = test.emotions().get('positivity')
   if guess < 0.5 :
       if 0 == row['label']:
	       correct = correct + 1
   elif guess > 0.5 :
       if 1 == row['label']:
	       correct = correct + 1    
print("Number of correct guess :" + str(correct) + " out of :" + str(10000))