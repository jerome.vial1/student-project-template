# Ce script permet d'analyser la correlation des résultats entre PyFeel et Google CNL
# pour un dataset donné. Cela permet de connaitre le nombre de commentaires analysés de la 
# même manière


# importing library
import pandas as pd
from pyFeel import Feel
import numpy as np
from google.cloud import language_v1

##############################################PyFeel############################################################
np_load_old = np.load
# modify the default parameters of np.load
np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)

# Then loading csv file
df = pd.read_csv('ytb_lecisc.csv')

# Declare the two list, we need to specify the size
list_of_guess_pyfeel = [None] * len(df.index)
list_of_guess_cnl = [None] * len(df.index)

# Analyse each Entrie
ii = 0
for index, row in df.iterrows():
   test = Feel(row['text'])
   guess = test.emotions().get('positivity')
   
   if guess < 0.5 :
       list_of_guess_pyfeel[ii] = 0
   elif guess > 0.5 :
       list_of_guess_pyfeel[ii] = 1
   ii += 1	   
	   
##################################################################################################################
##############################################CNL#################################################################
# Execute this in terminal to set credentials
# set GOOGLE_APPLICATION_CREDENTIALS=C:\Users\jerom\Downloads\TestPS6-0f51637db470.json 
i = 0
def print_result(annotations):
    global i
    score = annotations.document_sentiment.score
    magnitude = annotations.document_sentiment.magnitude

    if score < 0.5 :
        list_of_guess_cnl[i] = 0
    elif score > 0.5 :
        list_of_guess_cnl[i] = 1
    i += 1
    return 0
	
def analyze(text_content):
    client = language_v1.LanguageServiceClient()

    type_ = language_v1.Document.Type.PLAIN_TEXT
    language = "fr"
    document = {"content": text_content, "type_": type_, "language": language}  

    encoding_type = language_v1.EncodingType.UTF8
    annotations = client.analyze_sentiment(request={'document': document, 'encoding_type': encoding_type})
    print_result(annotations)
	
# Analyse each Entrie 
for index, row in df.iterrows():
   analyze(row['text'])    
##################################Compare the correlation between the two list######################################
print(list_of_guess_pyfeel)
print("-----------------------------")
print(list_of_guess_cnl)

def return_indices_correlated_guess(a, b):
    return [i for i, v in enumerate(a) if v == b[i]]

# Print the index of the Entries that got the same score
print(return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl))
print("number of same guess:" + str(len(return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl))))
#print("-----------------------------")

# Print the Entrie itself
# /!\ don't forget to add the lowest boundarie
#print("------------------------------ Correlated Tweets ----------------------------------")
for i in return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl):
    #print(df._get_value((i+768408), 'text'))
	print(df._get_value((i), 'text'))