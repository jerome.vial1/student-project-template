# Ce script permet d'analyser des entrées d'un dataset avec Google CNL
# et d'analyser le pourcentage de réussite.

# importing library
import pandas as pd
from google.cloud import language_v1

#TODO execute this in terminal to set credentials
# set GOOGLE_APPLICATION_CREDENTIALS=C:\Users\jerom\Downloads\TestPS6-0f51637db470.json 

# Then loading csv file
df = pd.read_csv('french_tweets.csv')

# Store the number of analyse that match the polarity of the dataset +/- rtol
correct = 0

def print_result(annotations):
    score = annotations.document_sentiment.score
    magnitude = annotations.document_sentiment.magnitude

    for index, sentence in enumerate(annotations.sentences):
        sentence_sentiment = sentence.sentiment.score

    global correct 
    if score < 0.5 :
        if 0 == row['label']:
	        correct = correct + 1
    elif score > 0.5 :
        if 1 == row['label']:
	        correct = correct + 1	
    return 0
	
def analyze(text_content):
    """Run a sentiment analysis request on text within a passed filename."""
    client = language_v1.LanguageServiceClient()

    type_ = language_v1.Document.Type.PLAIN_TEXT
    language = "fr"
    document = {"content": text_content, "type_": type_, "language": language}  

    # Available values: NONE, UTF8, UTF16, UTF32
    encoding_type = language_v1.EncodingType.UTF8
    annotations = client.analyze_sentiment(request={'document': document, 'encoding_type': encoding_type})
    # Print the results
    print_result(annotations)

# Analyse each Tweet (0 -> 768308 first entries are negatives Tweet, then positives)
for index, row in df.iloc[:10000].iterrows():
   analyze(row['text'])    
print("Number of correct guess :" + str(correct) + " out of :" + str(10000))