Détection de sentiments dans des commentaires
=============


Prérequis
---------

- Python 3.9.2
- pip 20.2.3
- numpy 1.20.1


Installation
------------

Il est nécessaire d'installer PyFeel :

    pip install git+https://github.com/AdilZouitine/pyFeel --upgrade

Il est nécessaire de télécharger le dataset suivant : https://www.kaggle.com/hbaflast/french-twitter-sentiment-analysis
