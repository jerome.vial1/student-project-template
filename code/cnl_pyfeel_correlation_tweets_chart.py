# importing library
import pandas as pd
from pyFeel import Feel
import numpy as np
from google.cloud import language_v1
import matplotlib.pyplot as plt

# chaque algo rempli une liste où index de la liste = index du tweet et valeur = 1 si positif, 0 si negatif
# ensuite je compare les deux listes entre elles
# les enrteée de 0 à 768308 sont négatives, puis positives dans le dataset.

##############################################PyFeel############################################################
np_load_old = np.load
# modify the default parameters of np.load
np.load = lambda *a,**k: np_load_old(*a, allow_pickle=True, **k)

# Then loading csv file
df = pd.read_csv('french_tweets_split.csv')

# Declare the two list, we need to specify the size
size = 50
list_of_guess_pyfeel = [None] * size
list_of_guess_cnl = [None] * size


# Analyse each Tweet (0 -> 768308 first entries are negatives Tweet, then positives)
ii = 0
for index, row in df.iloc[:50].iterrows():
   test = Feel(row['text'])
   guess = test.emotions().get('positivity')
   
   if guess < 0.5 :
       list_of_guess_pyfeel[ii] = 0
   elif guess > 0.5 :
       list_of_guess_pyfeel[ii] = 1
   ii += 1	   
	   
##################################################################################################################
##############################################CNL#################################################################
# Execute this in terminal to set credentials
# set GOOGLE_APPLICATION_CREDENTIALS=C:\Users\jerom\Downloads\TestPS6-0f51637db470.json 
i = 0
def print_result(annotations):
    global i
    score = annotations.document_sentiment.score
    magnitude = annotations.document_sentiment.magnitude
    if score < 0.5 :
        list_of_guess_cnl[i] = 0
    elif score > 0.5 :
        list_of_guess_cnl[i] = 1
    i += 1
    return 0
	
def analyze(text_content):
    client = language_v1.LanguageServiceClient()

    type_ = language_v1.Document.Type.PLAIN_TEXT
    language = "fr"
    document = {"content": text_content, "type_": type_, "language": language}  

    encoding_type = language_v1.EncodingType.UTF8
    annotations = client.analyze_sentiment(request={'document': document, 'encoding_type': encoding_type})
    print_result(annotations)
	
# Analyse each Tweet (0 -> 768308 first entries are negatives Tweet, then positives) 
for index, row in df.iloc[:50].iterrows():
   analyze(row['text'])    
##################################Compare the correlation between the two list######################################
# These two list contains the score for each Tweet 
#print(list_of_guess_pyfeel)
#print("-----------------------------")
#print(list_of_guess_cnl)

# This method returns the index where the value is the same for the two list given in parameter
def return_indices_correlated_guess(a, b):
  return [i for i, v in enumerate(a) if v == b[i]]

# Print the index of the tweets that got the same score
#print(return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl))
#print("-----------------------------")

# Print the Tweet itself
# /!\ don't forget to add the lowest boundarie (+0 for negative, and +768408 for positives)
print("------------------------------ Correlated Tweets ----------------------------------")
print("number of rows" + str(len(df.index)))
for i in return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl):
    #print(df._get_value((i+768408), 'text'))
	print(df._get_value((i), 'text'))
	
# Display a pie chart for the results
# Camembert ----------------------------------------------------------------
# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = 'Similaire', 'Différent'
similaire = len(return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl))
different = size - len(return_indices_correlated_guess(list_of_guess_pyfeel,list_of_guess_cnl))
sizes = [similaire, different]
explode = (0, 0)

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
       shadow=True, startangle=90)
ax1.axis('equal')
plt.show()
